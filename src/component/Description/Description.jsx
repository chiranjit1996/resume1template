import React,{Component} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faInfoCircle} from '@fortawesome/free-solid-svg-icons';

import Speech from '../speech/Speech.jsx'

class Description extends Component{
   
	   constructor(props) {
		super(props);
	 
		this.state = { 
			
		};
	  }
	  componentDidMount() {
		
	  }
	  
	render(){
		const {graduation_description,description_visible} = this.props;
		console.log(description_visible,"==========================");
		return(
			<>{description_visible?
				<div className="description_hover">
				  <h3><i ><FontAwesomeIcon icon={faInfoCircle} /></i>Description Of Hover <span></span></h3>

				  <ul>
				  	<h1>{this.props.description_element}</h1>
					
				  </ul>
				</div>:<></>}
			</>
		)
	}
}


export default Description;