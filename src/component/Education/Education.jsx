import React,{Component} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBriefcase } from '@fortawesome/free-solid-svg-icons';
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import { faGraduationCap } from '@fortawesome/free-solid-svg-icons';

import { faDesktop } from '@fortawesome/free-solid-svg-icons';

import sample_data from '../../jsonData/data.json'
import ProgramSkills from '../Skills/ProgramSkills.jsx'

import Description from '../Description/Description.jsx'
import Speech from '../speech/Speech.jsx'
import {faArrowUp} from '@fortawesome/free-solid-svg-icons'

import {faArrowDown} from '@fortawesome/free-solid-svg-icons'
import $ from 'jquery';
import { faCode } from '@fortawesome/free-solid-svg-icons';
import {faInfoCircle} from '@fortawesome/free-solid-svg-icons';
import moment from 'moment'

import { Modal } from 'react-modal-overlay'

import 'react-modal-overlay/dist/index.css'
import ReactAudioPlayer from "react-audio-player";

class Education extends Component{
   
	   constructor(props) {
		super(props);
	  
		// Initializing the state 
		this.state = { 
			education_hover_element:'',
			main_date_from_json:sample_data,
			educationDetailsData:[],
			graduation_data:[],
			college_data:[],
			school_data:[],
			college_subject:'',
			college_location:'',
			college_strt_date:'',
			college_end_date:'',
			college_desc:'',
			graduation_college_name:'',
			graduation_college_location:'',
			graduation_college_strt_date:'',
			graduation_college_end_date:'',
			graduation_college_desc:'',
			school_type:'',
			school_start_date:'',
			school_location:'',
			school_end_date:'',
			school_desc:'',
			hoverIsPresent:false,
			hover_element:'',
			microPhoneButton:true,
			description_data:'',
			working_content:[],
			pageNumber:1,
			right_arrow_visible:false,
			left_arrow_visible:false,
			lengthOfWorkData:[],
			speechElement:[],
			isHoverElement:true,
			hover_text:[],
			hover_header:[],
			isProgramSkillOpen:true,
			isDescriptionOpen:'',
			audioFile:'',
			showModal: false
			
			
		};
	  }
	  componentDidMount() {
		
		this.getEducationData();
		this.getWorkData();
		
	  }
	  getEducationData = () =>{
	  	
	  	const {main_date_from_json} = this.state
	  	this.setState({educationDetailsData:main_date_from_json.education});
	  	//console.log(this.state.educationDetailsData[1],"after assign");
	  	//this.get_graduation_data();
	  	// this.get_college_date();
	  	// this.get_school_data();
	  }
	  get_graduation_data = ()=>{
	  	const {main_date_from_json}=this.state;

		main_date_from_json.education.map((element,index )=> {
		    // console.log(element,"element=============");
		    // console.log(index,"index=============");
		    if(index == 0){
		    	const {graduation_college_name,graduation_college_location,graduation_college_strt_date,graduation_college_end_date,graduation_college_desc} = this.state

			   	this.setState({
			   		graduation_college_name:element.institutionFirstPart,
					graduation_college_location:element.area,
					graduation_college_strt_date:element.startDate,
					graduation_college_end_date:element.endDate,
					graduation_college_desc:element.description

			   	})
		    }
		    else if(index == 1){
		    	this.setState({
			   		college_subject:element.subjectType,
					college_location:element.institutionLocation,
					college_start_date:element.startDate,
					college_end_date:element.endDate,
					college_desc:element.description,

			   	})
		    }else{
		    	this.setState({
			   		school_type:element.institutionThidPart,
					school_start_date:element.startDate,
					school_end_date:element.endDate,
					school_desc:element.description,
					school_location:element.institutionLocation

			   	})
		    }
		    
		});
		
	  }
	  get_college_date = ()=>{
	  	

	  }
	  get_school_data = ()=>{

	  }
	 
	  

	  getWorkData =() =>{
	  	let length_of_json=0
	  	sample_data.work.map((item)=>{
	  		length_of_json+=1
	  	})
	  	this.setState({lengthOfWorkData:length_of_json})
	  	let getFirstThreeData=sample_data.work.slice(0,this.state.pageNumber*3)
	  	if (length_of_json > 3){
	  	
	  		this.setState({working_content:getFirstThreeData
  						 ,right_arrow_visible:true},() => {
							    this.setDataOfDinamicState();
							})		  		
	  	}
	  	else{
	  		this.setState({working_content:getFirstThreeData})
	  	}
	  	
	  }
	  setDataOfDinamicState = () =>{
	  	
	  	const {working_content} = this.state
	  	
	  		working_content.map((item,idx)=>{
	  			
		  		let hover_state=`hoverIsPresent_${idx}`
		  		//console.log(hover_state,"hover state in hover state========================in map");
		  		this.setState({[hover_state]:false})
	  	})
	  }

	  goToNextPage =(page_number)=>{
	  	//console.log(page_number,"page_number====next=========");
	  	let next_data=sample_data.work.slice((((page_number-1)*3)),(3*(page_number)))

	  	this.setState({working_content:next_data,
  						 pageNumber:page_number,right_arrow_visible:true})
	  	if (3*(page_number)>= this.state.lengthOfWorkData){
	  		this.setState({
	  			right_arrow_visible:false,
	  			left_arrow_visible:true
	  		})
	  	}

	  }
	   
	  goToPreviousPage =(page_number)=>{
	  	//console.log(page_number,"page_number======prev=======");
	  	let prev_data=sample_data.work.slice((3*(page_number-1)),(3*(page_number)))

	  	this.setState({working_content:prev_data,
  						 pageNumber:page_number,
  						 left_arrow_visible:true})
	  	
	  	
	  	
	  	if (page_number > 1){

	  		this.setState({
	  			left_arrow_visible:true,
	  			//right_arrow_visible:true
	  		})

	  	}
	  	if (page_number == 1 ){
	  		this.setState({
	  			left_arrow_visible:false,
	  			right_arrow_visible:true
	  		})
	  	}

	  }
	  onHoverElemntForWork = (index,listitem) =>{
	  	//console.log(listitem,"==============hover_dataelement===value");
	  	let value_of_index=".work"+index.toString()
	  	let html_str_modal=<div><p>{listitem.company}</p><p>{listitem.start.position}</p><p>{moment(listitem.start.year_month).format("MMM YYYY")} - {moment(listitem.end.year_month).format("MMM YYYY")}</p><p>{listitem.summary}<ul>{listitem.highlights.map((dataoFHighlights,index) => (<li>{dataoFHighlights}</li>
	  		))}</ul></p>
	        </div>
	  	const {hoverIsPresent, hover_text,hover_header,audioFile} = this.state;
	  	import(`../../jsonData/audio/${listitem.audioName}`).then(audio =>{
	  		this.setState({audioFile:
	  		audio.default})
	  	})
	  	this.setState({hoverIsPresent:true,hover_element:html_str_modal})
	  	$(value_of_index).show();
	  	$(value_of_index).css('transform', 1.25);

	  }
	  removeHoverForWork = (index) =>{
	  	let value_of_index=".work"+index.toString()
	  	//console.log(value_of_hover_with_index,"=======================")
	  	$(value_of_index).hide();
	  	this.setState({hoverIsPresent:false,ProgramSkillsVisible:false})
	  }

	  onHoverElemnt = (index,eduItem) =>{

	  	
	  	//console.log(eduItem,"==============hover_dataelement===value");
	  	let value_of_index=".edu"+index.toString()
	  	let html_str_modal=<div><p>{eduItem.area}
	  	
	  	</p><p>{eduItem.institutionFirstPart}
	  	
		</p><p>{eduItem.institutionSecondPart}</p><p>{moment(eduItem.startDate).format("MMM YYYY")} - {moment(eduItem.endDate).format("MMM YYYY")}</p>
	    </div>
	  	//console.log(value_of_index,"=======================")
	  	const {hoverIsPresent, hover_text,hover_header} = this.state
	  	this.setState({hoverIsPresent:true,hover_element:html_str_modal})
	  	$(value_of_index).show();
	  }
	  removeElement = (index) =>{
	  	let value_of_index=".edu"+index.toString()
	  	//console.log(value_of_hover_with_index,"=======================")
	  	$(value_of_index).hide();
	  	this.setState({hoverIsPresent:false,ProgramSkillsVisible:false})
	  }

	  showDescription =() =>{
	  	console.log("description called");
	  	this.setState({isDescriptionOpen:true,isProgramSkillOpen:false})
	  }

	  toggleModal = () =>{
	  	this.setState({isDescriptionOpen:false})
	  }

	  openModal = () =>{
	  	this.setState({modalIsOpen:true})
	  }

	  closeModal = () =>{
	  	this.setState({modalIsOpen:false})
	  }
	  
	
	render(){
		
		//console.log(this.state.hoverIsPresent_0,"=============in render============")
		const page_number= this.state.pageNumber;
		let hover_value='hoverIsPresent_';
		//console.log(this.state.hoverIsPresent_0,"=============in r hover value ender============")
		const {graduation_college_name,school_desc,college_desc,graduation_college_desc,hover_element,graduation_college_location,hover_header,working_content,hoverIsPresent,isProgramSkillOpen,isDescriptionOpen,educationDetailsData,audioFile}= this.state
		//console.log(audioFile,"==========================audioFile")
		const bg = {
		   overlay: {
		     background: "#FFFF00"
		   }
		 }
		 const customStyles = {
		  content: {
		    top: '50%',
		    left: '50%',
		    right: 'auto',
		    bottom: 'auto',
		    marginRight: '-50%',
		    transform: 'translate(-50%, -50%)',
		  },
		};
		return(
			<>
			 
				<div className="work">
				  <h3><i><FontAwesomeIcon icon={faBriefcase} size={'5x'} /></i>Experience
				  		{this.state.right_arrow_visible?<i onClick={this.goToNextPage.bind(this,page_number+1)} style={{float: 'right'}}><FontAwesomeIcon icon={faArrowDown} /></i>:''}
				 		{this.state.left_arrow_visible?<i onClick={this.goToPreviousPage.bind(this,page_number-1)} style={{float: 'right'}} ><FontAwesomeIcon icon={faArrowUp} /></i>:''}
				  </h3>
				  
				  <ul>
					{working_content.map((listitem,index) => (
	            		<li onMouseEnter={this.onHoverElemntForWork.bind(this,index,listitem)} onMouseLeave={this.removeHoverForWork.bind(this,index)}><span>{listitem.company}{<button className={`work${index}`} style={{display:"none",border:"none",float:"right",background:"none"}}><Speech
	            			speech_text={audioFile}
            				/></button>}{<button className={`work${index}`} style={{display:"none",float:"right",border:"2px solid #fdd835",width:"25px",height:"24px", borderRadius:"50%", color:"#fdd835",background: "none",marginTop: "5px"}} onClick={this.showDescription}><i><FontAwesomeIcon icon={faInfo} /></i></button>}</span><small>{listitem.start.position}</small><small>{moment(listitem.start.year_month).format("MMM YYYY")} - {moment(listitem.end.year_month).format("MMM YYYY")}</small>
	            		</li>
		          ))}
				  </ul>
				</div>
				
				<div className="edu">
				  <h3><i><FontAwesomeIcon icon={faGraduationCap} /></i>Education</h3>
				  <ul>
					
					{educationDetailsData.map((eduItem,index) => (
	            		<li onMouseEnter={this.onHoverElemnt.bind(this,index,eduItem)} onMouseLeave={this.removeElement.bind(this,index)}><span>{eduItem.area}{<button className={`edu${index}`} style={{display:"none",border:"none",float:"right",background:"none"}}><Speech
	            			speech_text={eduItem.area}
            				/></button>}{<button className={`edu${index}`} style={{display:"none",float:"right",border:"2px solid #fdd835",width:"25px",height:"24px", borderRadius:"50%", color:"#fdd835",background: "none",marginTop: "5px"}} onClick={this.showDescription}><i><FontAwesomeIcon icon={faInfo} /></i></button>}</span><small>{eduItem.institutionFirstPart}</small><small>{eduItem.institutionSecondPart}</small><small>{moment(eduItem.startDate).format("MMM YYYY")} - {moment(eduItem.endDate).format("MMM YYYY")}</small>
	            		</li>
		          ))}
					
				  </ul>
				</div>

				{isDescriptionOpen ?
					<div style={{ marginTop: '100px' ,background:'black'}}>
        
				        <Modal show={isDescriptionOpen} closeModal={this.toggleModal} styles={bg}>
				        	
				        	<p style={{marginTop: "10px"}}><i style={{float: 'left'}}><FontAwesomeIcon icon={faDesktop}/></i><span style={{float: 'left',marginLeft: "12px"}}>Description </span>
				        	<span ><ReactAudioPlayer
							      src={audioFile}
							      autoPlay={false}
							      width={600}
  								  height={400}
							      controls
							    />
							</span>
							</p>
				        	<hr style={{ background: '#fdd835'}}></hr>
	          				<small> {this.state.hover_element} </small>
	       				 </Modal>
				      </div>
				:
		        ''}
		              
				<ProgramSkills
					description_element={''}
					isProgramSkillOpen={isProgramSkillOpen}
				/>
				
				
				
			</>
		)
	}
}

export default Education