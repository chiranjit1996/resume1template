import React,{Component} from 'react'
import  './Home.scss'
import sample_data from '../../jsonData/data.json'
import colorData from '../../jsonData/color.json'
import SideBar from '../Sidebar/SideBar.jsx'
import Education from '../Education/Education.jsx'
import SoftSkills from '../Skills/SoftSkills.jsx'
import Interests from '../Interests/Interests.jsx'



class Home extends Component{
   
	   constructor(props) {
		super(props);
	  
		// Initializing the state 
		this.state = { 
			resume_data:sample_data,
			curColorSchema:0,
			colorSchema:[["#1a237e","#283593"],
						["#f96161", "#fce77d"],
						["#fce77d", "#f96161"],
						["#f9d342", "#292826"],
						["#292826", "#f9d342"],
						["#df678c", "#4C167A"],
						["#4C167A", "#df678c"],
						["#ccf381", "#4831d4"],
						["#4831d4", "#ccf381"],
						["#4a274f", "#f0a07c"],
						["#f0a07c", "#4a274f"],
						["#fad744", "#ef5455"],
						["#ef5455", "#fad744"],
						["#fff748", "#3c1a5b"],
						["#3c1a5b", "#fff748"],
						["#2f3c7e", "#fbeaeb"],
						["#fbeaeb", "#2f3c7e"],
						["#ec4d37", "#1d1b1b"],
						["#1d1b1b", "#ec4d37"],
						["#8bd8bd", "#243665"],
						["#243665", "#8bd8bd"],
						["#141a46", "#ec8b5e"],
						["#243665", "#ec8b5e"],
						["#1F2029", "#ffe67c"],
						["#ffe67c", "#1F2029"],
						["#f4a950", "#161b21"],
						["#161b21", "#f4a950"],
						["#eb2188", "#080a52"],
						["#080a52", "#eb2188"],
						["#4a171e", "#EFEFEF"]]
		};
	  }
	  componentDidMount() {
	  }
	  

	  myHandler = () => {
	  	const {curColorSchema,colorSchema} = this.state
	  	console.log(colorSchema.length ,"color scheema length");
	  	if (curColorSchema == colorSchema.length){
	  		this.setState({curColorSchema:0})
	  	}
	  	else{
	  	this.setState({curColorSchema:curColorSchema+1})
		}
	  	//  
	  }
	  resultantData =()=>{

	  }
	render(){
		//const data_of_json = this.state.resume_data;
		const {curColorSchema,colorSchema} = this.state
		return(
			<>
			<div className="resume">
				<div className="base">
					<SideBar/>
					<div ><button className="change_tempelate_color" onClick={this.myHandler}>Click To Change Color</button>
					</div>
				</div>				
				<div className="func">					
					<Education/>				
					<SoftSkills/>
					<Interests/>
					
				</div>
			
			</div>
			<style>
				{`
										
					.educ-chose-item-button {
						color: ${ colorData[curColorSchema] in colorSchema ? colorSchema[colorData[curColorSchema]][1] : colorSchema[0][1]};
					}

					
					.resume .base {
						background: ${ colorData[curColorSchema] in colorSchema ? colorSchema[colorData[curColorSchema]][1] : colorSchema[0][1]};
					}



					.resume .func .skills-prog {
						background: ${ colorData[curColorSchema] in colorSchema ? colorSchema[colorData[curColorSchema]][1] : colorSchema[0][1]};
					}
					.resume .func .work , 
					.resume .func .edu ,
					.resume .func .skills-soft,
					.resume .func .interests {
						background: ${ colorData[curColorSchema] in colorSchema ? colorSchema[colorData[curColorSchema]][1] : colorSchema[0][1]};
					}

					
					.resume,
					.header-switch {
						background: ${ colorData[curColorSchema] in colorSchema ? colorSchema[colorData[curColorSchema]][0] : colorSchema[0][0]};
					}

					.work-link:hover,
					.header-switch-btn-active {
						color: ${ colorData[curColorSchema] in colorSchema ? colorSchema[colorData[curColorSchema]][0] : colorSchema[0][0]};
					}

					
				`}
			</style>
			
			</>
			
		)
	}
}


export default Home;