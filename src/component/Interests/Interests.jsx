import React,{Component} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faBook } from '@fortawesome/free-solid-svg-icons';
import { faHeadphones } from '@fortawesome/free-solid-svg-icons';
import { faFilm } from '@fortawesome/free-solid-svg-icons';
import { faGamepad } from '@fortawesome/free-solid-svg-icons';
import { faPalette } from '@fortawesome/free-solid-svg-icons';

class Interests extends Component{
   
	   constructor(props) {
		super(props);
	  
		// Initializing the state 
		this.state = { 
			
		};
	  }
	  componentDidMount() {
		//alert("coming");
		
	  }
	  currentpage =()=>{
	  	//console.log(1);
	  }
	  
	render(){
		//console.log(this.props.hoverChange,"in interess");
		return(
			<>
				<div className="interests">
				  <h3><i><FontAwesomeIcon icon={faStar} /></i>Interests</h3>
				  <div className="interests-items">
					<div className="art"><i><FontAwesomeIcon icon={faPalette} /></i><span>Art</span></div>
					<div className="art"><i><FontAwesomeIcon icon={faBook} /></i><span>Books</span></div>
					<div className="movies"><i > <FontAwesomeIcon icon={faFilm} /></i><span>Movies</span></div>
					<div className="music"><i> <FontAwesomeIcon icon={faHeadphones} /></i><span>Music</span></div>
					<div className="games"><i > <FontAwesomeIcon icon={faGamepad} /></i><span>Games</span></div>
				  </div>
				</div>
				
			</>
		)
	}
}


export default Interests;