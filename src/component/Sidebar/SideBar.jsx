import React,{Component} from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRocket } from '@fortawesome/free-solid-svg-icons';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faLocationPin } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faMapMarker } from '@fortawesome/free-solid-svg-icons';
import { faBriefcase } from '@fortawesome/free-solid-svg-icons';
import { faGraduationCap } from '@fortawesome/free-solid-svg-icons';
import { faCode } from '@fortawesome/free-solid-svg-icons';
import { faBezierCurve } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faPinterest } from '@fortawesome/free-brands-svg-icons'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { faCodepen } from '@fortawesome/free-brands-svg-icons'
import { faBehance } from '@fortawesome/free-brands-svg-icons'
import { faBook } from '@fortawesome/free-solid-svg-icons';
import { faHeadphones } from '@fortawesome/free-solid-svg-icons';
import { faFilm } from '@fortawesome/free-solid-svg-icons';
import { faGamepad } from '@fortawesome/free-solid-svg-icons';
import { faPalette } from '@fortawesome/free-solid-svg-icons';

import sample_data from '../../jsonData/data.json'



class SideBar extends Component{
   
	   constructor(props) {
		super(props);
	  
		// Initializing the state 
		this.state = { 
			mainResumeData:sample_data
		};
	  }
	  componentDidMount() {
	
	  }
	  
	render(){
		const {mainResumeData} = this.state;
		return(
			
				<>
			  
				<div className="profile">
				  <div className="photo">
					 <FontAwesomeIcon icon={faRocket} />
				  </div>
				  <div className="info">
					<h1 className="name">Naomi Weatherford</h1>
					<h2 className="job">Frontend Web Designer</h2>
				  </div>
				</div>
				<div className="about">
				  <h3>About Me</h3>{mainResumeData.basics.summary}
				</div>
				<div className="contact">
				  <h3>Contact Me</h3>
				  <div className="call"><a href={mainResumeData.basics.telephoneContacts[0].link}> <i><FontAwesomeIcon icon={faPhone} /></i><span>{mainResumeData.basics.telephoneContacts[0].number}</span></a></div>
				  <div className="call"><a href={mainResumeData.basics.mobilePhoneContacts[0].link}> <i><FontAwesomeIcon icon={faPhone} /></i><span>{mainResumeData.basics.mobilePhoneContacts[0].number}</span></a></div>
				  <div className="address"> <a href="https://goo.gl/maps/fiTBGT6Vnhy"><i><FontAwesomeIcon icon={faMapMarker} /></i><span>{mainResumeData.basics.location}</span></a>
				  </div>
				  <div className="email"><a href={mainResumeData.basics.yahooContacts[0].link}><i><FontAwesomeIcon icon={faEnvelope} /></i><span>{mainResumeData.basics.yahooContacts[0].link}</span></a></div>
				  <div className="website"><a href="http://astronaomical.com/" target="_blank"><i> <FontAwesomeIcon icon={faHome} /></i><span>astronaomical.com</span></a></div>
				</div>
				<div className="follow">
				  <h3>Follow Me</h3>
				  <div className="box">
					<a href="https://www.facebook.com/astronaomical/" target="_blank"><i><FontAwesomeIcon icon={faFacebook} /></i></a>
					<a href="https://www.instagram.com/astronaomical/" target="_blank"><i><FontAwesomeIcon icon={faInstagram} /></i></a>
					<a href="https://www.pinterest.com/astronaomical/" target="_blank"><i><FontAwesomeIcon icon={faPinterest} /></i></a>
					<a href="https://www.linkedin.com/in/naomi-weatherford-758385112/" target="_blank"><i><FontAwesomeIcon icon={faLinkedin} /></i></a>
					<a href="https://codepen.io/astronaomical/" target="_blank"><i><FontAwesomeIcon icon={faCodepen} /></i></a>
					<a href="https://www.behance.net/astronaomical" target="_blank"><i><FontAwesomeIcon icon={faBehance} /></i></a>
				  </div>
				</div>
			  
			  </>
					 
		
		)
	}
}


export default SideBar;