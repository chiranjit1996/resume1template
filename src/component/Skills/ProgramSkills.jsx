import React,{Component} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faCode } from '@fortawesome/free-solid-svg-icons';

import $ from 'jquery';
import sample_data from '../../jsonData/data.json'
import ProgressBar from "@ramonak/react-progress-bar";
import Collapsible from 'react-collapsible';

import {faAnglesUp} from '@fortawesome/free-solid-svg-icons';
import { faAnglesDown } from '@fortawesome/free-solid-svg-icons';

import {faArrowUp} from '@fortawesome/free-solid-svg-icons'

import {faArrowDown} from '@fortawesome/free-solid-svg-icons'

class ProgramSkills extends Component{
   
	   constructor(props) {
		super(props);
	  
		// Initializing the state 
		this.state = { 
			//ProgramSkillsVisible:false
			mainDataofProgramSkill:[],
			colapsable:true,
			isDescriptionOpen:false,
			pageNumber:1,
			programskill_content:[],
			lengthOfProgramSkillData:[],
			right_arrow_visible:false,

			left_arrow_visible:false,
			openClopsible:true


		};
	  }
	  componentDidMount() {
		//alert("coming");
		this.getProgramSkillData()
		this.percentageDataForProgSkills()
		const {ProgramSkillsVisible,description_element,hover_header_details,isProgramSkillOpen} = this.props;
		this.setState({isDescriptionOpen:isProgramSkillOpen})
		
	  }
	  getProgramSkillData = () =>{
	  	//console.log(sample_data.skills,"=====================in prog")
	  	this.setState({mainDataofProgramSkill:sample_data.skills})
	  	let length_of_json=0
	  	sample_data.skills.map((item)=>{
	  		length_of_json+=1
	  	})
	  	this.setState({lengthOfProgramSkillData:length_of_json})
	  	let getFirstSevenData=sample_data.skills.slice(0,this.state.pageNumber*7)
	  	if (length_of_json > 7){
	  	
	  		this.setState({programskill_content:getFirstSevenData
  						 ,right_arrow_visible:true},() => {
							    //this.setDataOfDinamicState();
							})		  		
	  	}
	  	else{
	  		this.setState({programskill_content:getFirstSevenData})
	  	}

	  }

	  getFirstSevenData = () =>{

	  }

	  percentageDataForProgSkills=()=>{
	  	$(".skills-prog li")
	    .find(".skills-bar")
	    .each(function(i) {
	      $(this)
	        .find(".bar")
	        .delay(i * 150)
	        .animate(
	          {
	            width:
	              $(this)
	                .parents()
	                .attr("data-percent") + "%"
	          },
	          1000,
	          "linear",
	          function() {
	            return $(this).css({
	              "transition-duration": ".5s"
	            });
	          }
	        );
	    });
	  }
	  close_the_colapsible = ()=>{
	  	//console.log("close the colapsible coming");
	  	const {ProgramSkillsVisible,description_element,hover_header_details,isProgramSkillOpen} = this.props;
	  	this.setState({isDescriptionOpen:false,openClopsible:false})
	  	console.log("close the hide coming");
	  	
	  }
	  open_the_colapsible =() =>{
	  	console.log("open the colapsible coming");
	  	const {ProgramSkillsVisible,description_element,hover_header_details,isProgramSkillOpen} = this.props;
	  	this.setState({isDescriptionOpen:true,openClopsible:true})
	  }

	  goToNextPage =(page_number)=>{
	  	//console.log(page_number,"page_number====next=========");
	  	const {isProgramSkillOpen} = this.props;
	  	let next_data=sample_data.skills.slice((((page_number-1)*7)),(7*(page_number)))
	  	console.log(next_data,"======================programskill_content next data");
	  	this.setState({programskill_content:next_data,
  						 pageNumber:page_number,right_arrow_visible:true})
	  	if (7*(page_number)>= this.state.lengthOfProgramSkillData){
	  		this.setState({
	  			right_arrow_visible:false,
	  			left_arrow_visible:true
	  		})
	  	}

	  }

	  goToPreviousPage =(page_number)=>{
	  	//console.log(page_number,"page_number======prev=======");
	  	let prev_data=sample_data.skills.slice((7*(page_number-1)),(7*(page_number)))

	  	this.setState({programskill_content:prev_data,
  						 pageNumber:page_number,
  						 left_arrow_visible:true
  						})
	  	
	  	
	  	
	  	if (page_number > 1){

	  		this.setState({
	  			left_arrow_visible:true,
	  			//right_arrow_visible:true
	  		})

	  	}
	  	if (page_number == 1 ){
	  		this.setState({
	  			left_arrow_visible:false,
	  			right_arrow_visible:true
	  		})
	  	}

	  }
	  
	render(){
		const {ProgramSkillsVisible,description_element,hover_header_details,isProgramSkillOpen} = this.props;

		const {mainDataofProgramSkill,isDescriptionOpen,programskill_content,openClopsible}= this.state
		console.log(openClopsible,"=============openClopsible");
		const page_number= this.state.pageNumber;
		//ProgramSkillsVisible =!ProgramSkillsVisible
		return(
		
			<>
				
				<div className="skills-prog">
				<h3><i><FontAwesomeIcon icon={faCode} /></i>Programming Skills{isDescriptionOpen?<i onClick={this.close_the_colapsible} style={{float: 'right'}} ><FontAwesomeIcon icon={faAnglesUp} /></i>:<i onClick={this.open_the_colapsible } style={{float: 'right'}}><FontAwesomeIcon icon={faAnglesDown} /></i>}{this.state.right_arrow_visible?<i onClick={this.goToNextPage.bind(this,page_number+1)} style={{float: 'right'}}><FontAwesomeIcon icon={faArrowDown} /></i>:''}
				 		{this.state.left_arrow_visible?<i onClick={this.goToPreviousPage.bind(this,page_number-1)} style={{float: 'right'}} ><FontAwesomeIcon icon={faArrowUp} /></i>:''}
				  </h3>
				<Collapsible 
				open={isDescriptionOpen}
				>
				  
			      <ul>
					
					{programskill_content.map((listitem,index) => (
	            		<li><span>{listitem.name}</span>
	            		
						 	<ProgressBar  
						 		completed={listitem.knowlege_have[0]}
						 		className="wrapper"
						 		barContainerClassName="container"
						 		height={"4px"}
						 		bgColor={"#ffb300"}
						 		isLabelVisible={false}
						 		animateOnRender={true}
						 	/>
					 		 
						</li>
		            ))}
					
				  </ul>
				  </Collapsible>
				  </div>
				  
			    
			</>
		)
	}
}


export default ProgramSkills;