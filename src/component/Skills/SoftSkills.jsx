import React,{Component} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBezierCurve } from '@fortawesome/free-solid-svg-icons';
import $ from 'jquery';


class SoftSkills extends Component{
   
	   constructor(props) {
		super(props);
	  
		// Initializing the state 
		this.state = { 
			
		};
	  }
	  componentDidMount() {
		//alert("coming");
		this.percentageDataForSoftSkills()
		
	  }
	  percentageDataForSoftSkills =() =>{
	  $(".skills-soft li")
	    .find("svg")
	    .each(function(i) {
	      var c, cbar, circle, percent, r;
	      circle = $(this).children(".cbar");
	      r = circle.attr("r");
	      c = Math.PI * (r * 2);
	      percent = $(this)
	        .parent()
	        .data("percent");
	      cbar = (100 - percent) / 100 * c;
	      circle.css({
	        "stroke-dashoffset": c,
	        "stroke-dasharray": c
	      });
	      circle.delay(i * 150).animate(
	        {
	          strokeDashoffset: cbar
	        },
	        1000,
	        "linear",
	        function() {
	          return circle.css({
	            "transition-duration": ".3s"
	          });
	        }
	      );
	      $(this)
	        .siblings("small")
	        .prop("Counter", 0)
	        .delay(i * 150)
	        .animate(
	          {
	            Counter: percent
	          },
	          {
	            duration: 1000,
	            step: function(now) {
	              return $(this).text(Math.ceil(now) + "%");
	            }
	          }
	        );
	    });
	}
	  
	render(){
		return(
			<>
				<div className="skills-soft">
				  <h3><i ><FontAwesomeIcon icon={faBezierCurve} /></i>Software Skills</h3>
				  <ul>
					<li data-percent="90">
					  <svg viewBox="0 0 100 100">
						<circle cx="50" cy="50" r="45"></circle>
						<circle className="cbar" cx="50" cy="50" r="45"></circle>
					  </svg><span>Illustrator</span><small></small>
					</li>
					<li data-percent="75">
					  <svg viewBox="0 0 100 100">
						<circle cx="50" cy="50" r="45"></circle>
						<circle className="cbar" cx="50" cy="50" r="45"></circle>
					  </svg><span>Photoshop</span><small></small>
					</li>
					<li data-percent="85">
					  <svg viewBox="0 0 100 100">
						<circle cx="50" cy="50" r="45"></circle>
						<circle className="cbar" cx="50" cy="50" r="45"></circle>
					  </svg><span>InDesign</span><small></small>
					</li>
					<li data-percent="65">
					  <svg viewBox="0 0 100 100">
						<circle cx="50" cy="50" r="45"></circle>
						<circle className="cbar" cx="50" cy="50" r="45"></circle>
					  </svg><span>Dreamweaver</span><small></small>
					</li>
				  </ul>
				</div>
			</>
		)
	}
}


export default SoftSkills;