import React, { useState, useEffect } from "react";
import audiofile1 from '../../jsonData/audio/audio1.mp3'
import audiofile2 from '../../jsonData/audio/audio2.mp3'
import audiofile3 from '../../jsonData/audio/audio3.mp3'
import audiofile4 from '../../jsonData/audio/audio4.mp3'
import audiofile5 from '../../jsonData/audio/audio5.mp3'
import audiofile6 from '../../jsonData/audio/audio6.mp3'
import audiofile7 from '../../jsonData/audio/audio7.mp3'
import audiofile8 from '../../jsonData/audio/audio8.mp3'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import { faStop } from '@fortawesome/free-solid-svg-icons';

const useAudio = url => {
  const [audio] = useState(new Audio(url));
  console.log(url,"===========================");
  const [playing, setPlaying] = useState(false);

  const toggle = () => setPlaying(!playing);

  useEffect(() => {
      playing ? audio.play() : audio.pause();
    },
    [playing]
  );

  useEffect(() => {
    audio.addEventListener('ended', () => setPlaying(false));
    return () => {
      audio.removeEventListener('ended', () => setPlaying(false));
    };
  }, []);

  return [playing, toggle];
};

const Speech = ({ url }) => {
  console.log(url,"audio file type in props =========================")
  var audio_file_name = audiofile8;
  const [playing, toggle] = useAudio(audio_file_name);


  return (
    <div>
      <button class="playpauseBtn" onClick={toggle}>{playing ? <FontAwesomeIcon icon={faStop}/> : <FontAwesomeIcon icon={faPlay}/>}</button>
    </div>
  );
};

export default Speech;